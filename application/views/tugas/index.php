<!DOCTYPE html>
<html>
    <head>
        <title>SLICING TEST</title>
        
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" />
        <link rel="stylesheet" href="https://www.markuptag.com/bootstrap/5/css/bootstrap.min.css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

        <link rel="stylesheet" href="<?= base_url(); ?>assets/slicing/styleIndex.css">
        <style></style>
    </head>
    <body>
    <!--Heading-->
        <div class="container-fluid container-custom py-5" style="margin-bottom: 30px;">
        <header>
            <nav class="navbar navbar-expand-xl navbar-custom navbar-light h5">
                <div class="container">
                    <a class="navbar-brand" href="#">Delfwebmaker.co</a>
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact Us</a>
                            </li>
                        </ul>
                        <div class="d-flex">
                            <a href="<?= base_url('index.php/login'); ?>" class="btn btn btn-dark">Login</a>
                        </div>
                </div>
            </nav>
        </header>

        <!--Hero-->
            <div class="container py-5">
                <div class="row justify-content-start">
                    <div class="col-lg-8 text-center text-lg-start">
                    <h1>Cari jasa buat web? Tapi bingung kemana?</h1>
                    <p class="fs-4 text-dark mb-4">Tenang, kami dapat membantu membuat web sesuai dengan keinginanmu! Kami menawarkan jasa pembuatan web sesuai kebutuhanmu dan dengan biaya yang bikin kantong gak nangis!
                    <br>Tim kami merupakan tim profesional yang siap membantumu dalam pembuatan website.
                    <br><br> Ayo tunggu apalagi? Pesan sekarang</p>
                    <div class="pt-2">
                        <a href="" class="btn btn-secondary rounded-pill py-md-3 px-md-5 mx-2">Pesan Sekarang</a>
                        <a href="" class="btn btn-outline-secondary text-dark rounded-pill py-md-3 px-md-5 mx-2">Hubungi Kami</a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <img align="right" width="90%" src="<?= base_url(); ?>assets/slicing/png/hero.png">
                </div>
                </div>
            </div>
        </div>

    <!--Tabel Harga-->
    <div class="containerprice" style="margin-bottom: 100px;">
        <h2>Penawaran Untuk Anda</h2>
        <div class="price-row">
            <div class="price-col">
                <p>Paket A</p>
                <h3>Rp.xxx<br><span>Keuntungan:</span></h3>
                
                <ul>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                </ul>
                <button>Beli Sekarang!</button>
            </div>
            <div class="price-col">
                <p>Paket B</p>
                <h3>Rp.xxx<br><span>Keuntungan:</span></h3>
                
                <ul>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                </ul>
                <button>Beli Sekarang!</button>
            </div>
            <div class="price-col">
                <p>Paket C</p>
                <h3>Rp.xxx<br><span>Keuntungan:</span></h3>
                
                <ul>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                    <li>Lorem Ipsum dolor sit amet</li>
                </ul>
                <button>Beli Sekarang!</button>
            </div>
        </div>
    </div>

    <!--Klien-->
    <div class="boxs">
        <div class="row justify-content-start">
            <div class="col-lg-7 text-center text-lg-start">
                <h1 class="text-light">Ingin membuat website tetapi bingung cara pesannya?</h1>
                <br>
                <p class="fs-4 text-light mb-4">Tenang, kami akan membantu memandu anda agar anda dapat memesan web sesuai dengan pilihan anda.
                <br><br> Ayo tunggu apalagi? Pesan sekarang</p>
                <div class="pt-2">
                <a href="#" class="btns btns-secondary rounded-pill py-md-3 px-md-5 mx-2">Lihat Caranya</a>
                </div>
                </div>
                <div class="gambar col-lg-4">
                    <img align="right" width="90%" src="<?= base_url(); ?>assets/slicing/png/orang.png">
            </div>
        </div>
    </div>

    <!--Alasan-->
        <div class="client" style="margin-bottom: 250px;">
        <h2>Alasan Memilih Kami</h2>
        <div class="client-row">
            <div class="client-col text-dark">
                <h4>Alasan A</h4>
                <img src="<?= base_url(); ?>assets/slicing/png/oke.png" width="50%">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices erat nisi, ultricies placerat quam luctus nec.</p>
            </div>
            <div class="client-col text-dark">
                <h4>Alasan B</h4>
                <img src="<?= base_url(); ?>assets/slicing/png/oke.png" width="50%">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices erat nisi, ultricies placerat quam luctus nec.</p>
            </div>
            <div class="client-col text-dark">
                <h4>Alasan C</h4>
                <img src="<?= base_url(); ?>assets/slicing/png/oke.png" width="50%">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ultrices erat nisi, ultricies placerat quam luctus nec.</p>
            </div>
        </div>
    </div>

    <!--PortoFolio-->
    <div class="oval">
        <h2>Portofolio</h2>
        <div class="barisoval">
            <div class="porto" style="margin-top: 50px;">
                <div class="mid py-md-3">
                <h3>Portofolio x</h3>
                <br><h5>No dolore ipsum accusam no lorem. Invidunt sed clita kasd clita et et dolor sed dolor</h5>
                </div>
            </div>
            <div class="porto" style="margin-top: 30px;">
                <div class="mid py-md-3">
                <h3>Portofolio x</h3>
                <br><h5>No dolore ipsum accusam no lorem. Invidunt sed clita kasd clita et et dolor sed dolor</h5>
                </div>
            </div>
            <div class="porto" style="margin-top: 30px; margin-bottom: 100px">
                <div class="mid py-md-3">
                <h3>Portofolio x</h3>
                <br><h5>No dolore ipsum accusam no lorem. Invidunt sed clita kasd clita et et dolor sed dolor</h5>
                </div>
            </div>
        </div>
    </div>

        <!--Footer-->
        <div class="container-fluid bg-dark bg-footer text-light py-5">
        <div class="container py-5">
            <div class="row g-5">
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-custom">Delfwebmaker.co</h4>
                    <hr class="w-25 text-secondary mb-4" style="opacity: 1;">
                    <p class="mb-4">No dolore ipsum accusam no lorem. Invidunt sed clita kasd clita et et dolor sed dolor</p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-custom">Lainnya</h4>
                    <hr class="w-25 text-secondary mb-4" style="opacity: 1;">
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-light mb-2" href="#"><i class="fa fa-angle-right me-2"></i>About Us</a>
                        <a class="text-light mb-2" href="#"><i class="fa fa-angle-right me-2"></i>Beranda</a>
                        <a class="text-light mb-2" href="#"><i class="fa fa-angle-right me-2"></i>Portofolio</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-custom">Kontak</h4>
                    <hr class="w-25 text-secondary mb-4" style="opacity: 1;">
                    <div class="d-flex flex-column justify-content-start">
                        <a class="text-light mb-2" href="#"><i class="fa fa-angle-right me-2"></i>088xxxxxxxxx</a>
                        <a class="text-light mb-2" href="#"><i class="fa fa-angle-right me-2"></i>088xxxxxxxxx</a>
                        <a class="text-light mb-2" href="#"><i class="fa fa-angle-right me-2"></i>088xxxxxxxxx</a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h4 class="text-custom">Ikuti Kami</h4>
                    <hr class="w-25 text-secondary mb-4" style="opacity: 1;">
                    </form>
                    <div class="d-flex">
                        <a class="btn btn-lg btn-primary btn-lg-square rounded-circle me-2" href="#"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-lg btn-primary btn-lg-square rounded-circle me-2" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-lg btn-primary btn-lg-square rounded-circle me-2" href="#"><i class="fab fa-linkedin-in"></i></a>
                        <a class="btn btn-lg btn-primary btn-lg-square rounded-circle" href="#"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>